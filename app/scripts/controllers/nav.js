/**
 * Created by npatten on 1/4/14.
 */
'use strict';

angular.module('dorianKerschApp')
  .controller('NavCtrl', function ($scope, $location) {
    $scope.navClass = function (page) {
        var currentRoute = $location.path().substring(1) || 'home';
        return page === currentRoute ? 'active' : '';
    };
  });